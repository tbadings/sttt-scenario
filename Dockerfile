# Use Stormpy 1.6.4 docker container
FROM movesrwth/stormpy:1.6.4


# Activate virtual environment
############################
ENV VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"


# Build tool
#############
RUN mkdir /opt/upmdps
WORKDIR /opt/upmdps
# Obtain
COPY . .
# Build
RUN pip3 install --no-cache-dir -r requirements.txt
RUN python3 setup.py develop
