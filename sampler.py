'''
This is the main file of the Python implementation for the paper:

    Badings et al. (2022). Scenario-Based Verification of Uncertain Parametric
    MDPs. International Journal on Software Tools for Technology Transfer.
    
See the .README file in this folder for detailed instruction of how to use this
implementation.
'''

import numpy as np
import pandas as pd

from core.functions import ticDiff, tocDiff
from core.parser import parse_settings
from core.storm_interface import load_problem, sample_results
from core.compute_bounds import compute_avg_beta, compute_avg_eta
from core.plot import threshold_histogram

if __name__ == '__main__':
    np.random.seed(0)
    
    # Parse arguments provided via the command line
    Z = parse_settings()
    
    # Create buffer for output file
    buffer = ['OUTPUT FOR MODEL: '+str(Z['model']),'\n']
    
    # Load problem
    parameters, model, properties = load_problem(Z['model'], Z['property'],
                                                 Z['bisimulation'])

    # Determine how many parameter samples we need in total
    totalN = Z['iterations'] * max(Z['Nsamples'])
    
    # Sample solutions (using Storm) and save the results plus run time
    ticDiff()
    sols = sample_results(totalN, parameters, model, properties, 
                          Z['model'], Z['weather'])
    time = np.round(tocDiff(False) / totalN * 1000, 3)
    buffer += ['\n','Run time in Storm per 1000 samples: '+str(time),'\n']
    
    # Initialize pandas objects to store results in
    beta_results = pd.Series(name='Confidence probability (beta)', 
                             dtype='float64')
    eta_results = pd.DataFrame(index=Z['Nsamples'])
    
    # For every number of samples
    for n,N in enumerate(Z['Nsamples']):
        # For every comparator
        for c,C in enumerate(Z['comparator']):
    
            # We perform as much iterations as possible, given sample size N
            num_iter = int(np.floor(totalN / N))
            violated = np.zeros(num_iter)
            variable_threshold = np.zeros(num_iter)
            
            # For every iteration
            for i in range(num_iter):
                
                # Determine current slice in solutions array
                Slice = slice(i*N, (i+1)*N)
                
                # Threshold 'False' means that a variabel threshold is used,
                # in that case, apply Theorem 1 from the paper
                if type(Z['threshold']) == bool:
                    # In that case, number of violations is always zero
                    violated[i] = 0
                    
                    # Compute the threshold on the specification such that all
                    # samples are satisfying
                    if C == "leq":
                        variable_threshold[i] = max(sols[Slice])
                    else:
                        variable_threshold[i] = min(sols[Slice])
                    
                # Otherwise, apply Theorem 2 from the paper
                # Depending on comparator (leq/geq), compute nr of violations
                elif C == "leq":
                    violated[i] = np.sum(sols[Slice] > Z['threshold'])
                else:
                    violated[i] = np.sum(sols[Slice] < Z['threshold'])
            
            if type(Z['threshold']) == bool:
                # If a variable threshold was used, export these results as well
                threshold_histogram(variable_threshold, N,
                    outfile='data/'+Z['outfile']+'_histogram_C='
                    +str(C)+'.pdf')
            
            # Compute bounds on the confidence probability
            if Z['eta'] != [0]:
                print('\nCOMPUTE BETA BASED ON ETA (comparator: '+str(C)+')')
                
                key = str(N)+'-'+str(C)
                beta_results[key] = compute_avg_beta(violated, N, Z['eta'][c],
                                                     Z['threshold'])
            
                buffer += ['\n- Confidence probability for eta='+
                           str(Z['eta'][c])+' is beta='+str(beta_results[key])]
                
            # Compute bounds on the satisfaction probability
            if Z['beta'] != [0]:
                print('\nCOMPUTE ETA BASED ON BETA (comparator: '+str(C)+')')
                
                for bet in Z['beta']:
                    print('\nCompute eta for beta='+str(bet))
                    
                    key = str(bet)+'-'+str(C)
                    eta_results.loc[N,key] = compute_avg_eta(violated, N, bet,
                                                             Z['threshold'])
                    
                    buffer += ['\n- Lower bound sat.prob. for beta='+
                               str(bet)+' is eta='+str(eta_results[key][N])]
             
    # Write logfile
    f = open('data/'+Z['outfile']+'.txt', "w")
    f.writelines(buffer)
    f.close()
    
    # Export results for the confidence probability (if a sat.prob. was fixed)
    if Z['eta'] != 0:
        beta_results.to_csv('data/'+Z['outfile']+'_confprob.csv', sep=';')
    
    # Export results for the sat.prob. (if a confidence probability was fixed)    
    if Z['beta'] != 0:   
        eta_results.to_csv('data/'+Z['outfile']+'_satprob.csv', sep=';')
    
    print('\n---------------------\n')
