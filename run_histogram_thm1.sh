#!/bin/bash
echo -e "++++++++ CREATE THRESHOLD HISTOGRAMS (THEOREM 1) ++++++++\n";
python3 sampler.py --model models/brp/brp_rewards4_16_5.pm --property models/brp/brp_rewards4.prctl --bisimulation strong --comparator 'leq' --num_samples 1000 --num_iter 1000 --beta '0.99' --outfile 'brp_16-5_output_histogram_N=1k';
python3 sampler.py --model models/brp/brp_rewards4_16_5.pm --property models/brp/brp_rewards4.prctl --bisimulation strong --comparator 'leq' --num_samples 10000 --num_iter 1000 --beta '0.99' --outfile 'brp_16-5_output_histogram_N=10k';